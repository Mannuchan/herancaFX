package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FuncAdm extends Funcionario{
    private String setor;
    private String funcao;

    public FuncAdm(String nome, String endereco, int idade,double salario, String setor, String funcao, int codPes) {
        super(nome, endereco, idade, salario,codPes);
        this.setor=setor;
        this.funcao=funcao;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    @Override
    public void update() {
        tipo="Funcionário";
        super.update();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_FuncAdmPK SET codPes=?, salario=?, setor=?, funcao=? where codPes=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, codPes);
            preparedStatement.setDouble(2,salario);
            preparedStatement.setString(3,setor);
            preparedStatement.setString(4, funcao);
            preparedStatement.setInt(5,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete() {
        super.delete();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "DELETE OO_FuncAdmPK where codPes=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi excluido da tabela!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void insert() {
        tipo="Funcionário";
        super.insert();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO OO_FuncAdmPK (codPes,salario,setor,funcao) VALUES (?,?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.setDouble(2,salario);
            preparedStatement.setString(3, setor);
            preparedStatement.setString(4,funcao);
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    static public ArrayList<Pessoa> getAll(){ // Retorna um array contendo todas as pessoas no banco de dados
        
        Conexao c = new Conexao();
        ArrayList<Pessoa> al = new ArrayList<Pessoa>();
        Connection con = c.getConexao();
        String sql = "select * from OO_FuncAdmPK p1,OO_PessoaPK p2 where p1.codpes=p2.codpes";  // pega tudo oq tem dentro
        try {
            Statement s=con.createStatement(); // cria statement com a conexao
            ResultSet r=s.executeQuery(sql); // executa a string no statement
            while(r.next()){ // passa pelo arraylist
                FuncAdm l = new FuncAdm(r.getString("nome"), r.getString("endereco"), r.getInt("idade"),r.getDouble("salario"),r.getString("setor"), r.getString("funcao"),r.getInt("codPes"));
                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
}
}

