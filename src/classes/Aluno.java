package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Aluno extends Pessoa{
    private String semestre;
    private String curso;

    public Aluno(String nome, String endereco,int idade,String semestre, String curso, int codPes){
        super(nome, endereco, idade,codPes);
        this.semestre=semestre;
        this.curso=curso;
    }
    
    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public void update() {
        tipo="Aluno";
        super.update();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_AlunoPK SET codPes=?, semestre=?, curso=? where codPes=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.setString(2,semestre);
            preparedStatement.setString(3,curso);
            preparedStatement.setInt(4,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete() {
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "delete from OO_ProfessorPK p1,OO_PessoaPK p2 where p1.codpes=p2.codpes";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi excluido da tabela!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void insert() {
        tipo="Aluno";
        super.insert();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO OO_AlunoPK (codPes,semestre,curso) VALUES (?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.setString(2, semestre);
            preparedStatement.setString(3,curso);
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public String getFuncao() {
        return "Aluno";
    }
      static public ArrayList<Pessoa> getAll(){ // Retorna um array contendo todas as pessoas no banco de dados    
        Conexao c = new Conexao();
        ArrayList<Pessoa> al = new ArrayList<Pessoa>();
        Connection con = c.getConexao();
        PreparedStatement preparedStatement = null;
        String sql = "select * from OO_AlunoPK p1,OO_PessoaPK p2 where p1.codpes=p2.codpes";  // pega tudo oq tem dentro
        try {
            Statement s=con.createStatement(); // cria statement com a conexao
            ResultSet r=s.executeQuery(sql); // executa a string no statement
            while(r.next()){ // passa pelo arraylist
                Aluno l = new Aluno(r.getString("nome"), r.getString("endereco"), r.getInt("idade"),r.getString("semestre"), r.getString("curso"),r.getInt("codPes"));
                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
}

}
