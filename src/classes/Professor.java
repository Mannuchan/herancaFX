package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Professor extends Funcionario{
    private String disciplina;

    public Professor(String nome, String endereco, int idade, double salario,String disciplina, int codPes) {
        super(nome, endereco, idade, salario,codPes);
        this.disciplina=disciplina;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public void update() {
        tipo="Professor";
        super.update();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_ProfessorPK SET codPes=?, salario=?, disciplina=? where codPes=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.setDouble(2,salario);
            preparedStatement.setString(3,disciplina);
            preparedStatement.setInt(4,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete() {
        super.delete();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "DELETE from OO_ProfessorPK where codPes=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi excluido da tabela!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

   @Override
    public void insert() {
        tipo="Professor";
        super.insert();
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO OO_ProfessorPK (codPes,salario,disciplina) VALUES (?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.setDouble(2,salario);
            preparedStatement.setString(3,disciplina);
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public String getFuncao() {
        return "Professor";
    }
      static public ArrayList<Pessoa> getAll(){ // Retorna um array contendo todas os professores no banco de dados
        Conexao c = new Conexao();
        ArrayList<Pessoa> al = new ArrayList<Pessoa>();
        Connection con = c.getConexao();
        PreparedStatement preparedStatement = null;
        String sql = "select * from OO_ProfessorPK p1,OO_PessoaPK p2 where p1.codpes=p2.codpes";  // pega tudo oq tem dentro
        try {
            Statement s =con.createStatement(); // cria statement com a conexao
            ResultSet r =s.executeQuery(sql); // executa a string no statement
            while(r.next()){ // passa pelo arraylist
                Professor l = new Professor(r.getString("nome"), r.getString("endereco"), r.getInt("idade"),r.getDouble("salario"),r.getString("disciplina"),r.getInt("codPes"));
                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
}
}
