package classes;

public abstract class Funcionario extends Pessoa {
    protected double salario;

    public Funcionario(String nome, String endereco,int idade, double salario, int codPes){
        super(nome, endereco, idade,codPes);
        this.salario=salario;
    }
    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
