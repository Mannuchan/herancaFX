package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public abstract class Pessoa {
    protected String nome;
    protected String endereco;
    protected int idade;
    protected String funcao,tipo;
    protected int codPes;
    
    public Pessoa(String nome,String endereco,int idade,int codPes){
        this.nome=nome;
        this.endereco=endereco;
        this.idade=idade;
        this.codPes=codPes;
    }
    
    public void update(){
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "UPDATE OO_PessoaPK SET nome=?, codPes=?, idade=?, endereco=?,tipo=? where nome=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1,nome);
            preparedStatement.setInt(2, codPes);
            preparedStatement.setInt(3,idade);
            preparedStatement.setString(4,endereco);
            preparedStatement.setString(5,tipo);
            preparedStatement.setString(6,nome);
            preparedStatement.executeUpdate();
            System.out.println("Foi atualizado!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void delete(){
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "DELETE from OO_PessoaPK where codPes=?";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.executeUpdate();
            System.out.println("Foi excluido da tabela!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void insert() {
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO OO_PessoaPK (codPes,nome,endereco,idade,tipo) VALUES (?,?,?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1,codPes);
            preparedStatement.setString(2, nome);
            preparedStatement.setString(3,endereco);
            preparedStatement.setInt(4,idade);
            preparedStatement.setString(5,tipo);
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }    
    }
    
    public abstract String getFuncao();
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodPes() {
        return codPes;
    }

    public void setCodPes(int codPes) {
        this.codPes = codPes;
    }
    static public void deleteall() {
        Conexao c= new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "DELETE FROM OO_PessoaPK";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.executeUpdate();
            System.out.println("Foi excluido da tabela PessoaPK!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

}
