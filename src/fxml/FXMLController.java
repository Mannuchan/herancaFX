package fxml;

import classes.Aluno;
import classes.FuncAdm;
import classes.Pessoa;
import classes.Professor;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

public class FXMLController implements Initializable {
    
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, ?> nomeCol;
    @FXML
    private TableColumn<Pessoa, ?> idadeCol;
    @FXML
    private TableColumn<Pessoa, ?> enderecoCol;
    @FXML
    private TableColumn<Pessoa, ?> funcaoCol;
    
    private ObservableList<Pessoa> pessoas;
    @FXML
    private TextField nomeTextField;
    @FXML
    private TextField idadeTextField;
    @FXML
    private TextField enderecoTextField;
    @FXML
    private TextField funcaoTextField;
    @FXML
    private ToggleGroup a;
    @FXML
    private TextField salarioTextField;
    @FXML
    private TextField semestreTextField;
    @FXML
    private TextField cursoTextField;
    @FXML
    private TextField disciplinaTextField;
    @FXML
    private RadioButton funcionarioRadioButton;
    @FXML
    private RadioButton alunoRadioButton;
    @FXML
    private RadioButton professorRadioButton;
    @FXML
    private Label salarioLabel;
    @FXML
    private Label semestreLabel;
    @FXML
    private Label cursoLabel;
    @FXML
    private Label disciplinaLabel;
    @FXML
    private TextField setorTextField;
    @FXML
    private Label setorLabel;
    @FXML
    private Label funcaoLabel;
    @FXML
    private Label aviso;
    @FXML
    private TextField codPesTextField;
    @FXML
    private TableColumn<Pessoa, ?> codPesCol;
    
    @FXML
    private void cadastra(ActionEvent event) {   
        //cadastrando Aluno
        if (alunoRadioButton.isSelected()){ 
            Pessoa l=new Aluno(nomeTextField.getText(),enderecoTextField.getText(),Integer.valueOf(idadeTextField.getText()),semestreTextField.getText(),cursoTextField.getText(),Integer.valueOf(codPesTextField.getText()));
            l.setFuncao("Aluno");
            tabela.getItems().add(l);  
            l.insert();
            idadeTextField.clear();
            enderecoTextField.clear();
            nomeTextField.clear();  
            semestreTextField.clear(); 
            cursoTextField.clear(); 
            codPesTextField.clear(); 
        }
        //cadastrando Funcionario
        if (funcionarioRadioButton.isSelected()){
            Pessoa l=new FuncAdm(nomeTextField.getText(),enderecoTextField.getText(),Integer.valueOf(idadeTextField.getText()),Double.valueOf(salarioTextField.getText()),setorTextField.getText(),funcaoTextField.getText(), Integer.valueOf(codPesTextField.getText()));
            l.setFuncao(funcaoTextField.getText());
            tabela.getItems().add(l);  
            l.insert();
            idadeTextField.clear();
            enderecoTextField.clear();
            nomeTextField.clear(); 
            salarioTextField.clear(); 
            setorTextField.clear(); 
            funcaoTextField.clear();
            codPesTextField.clear(); 
        }
        //cadastrando Professor
        if (professorRadioButton.isSelected()){
            Pessoa l=new Professor(nomeTextField.getText(),enderecoTextField.getText(),Integer.valueOf(idadeTextField.getText()),Double.valueOf(salarioTextField.getText()),disciplinaTextField.getText(), Integer.valueOf(codPesTextField.getText()));
            l.setFuncao("Professor");
            tabela.getItems().add(l);  
            l.insert();
            idadeTextField.clear();
            enderecoTextField.clear();
            nomeTextField.clear(); 
            salarioTextField.clear(); 
            disciplinaTextField.clear(); 
            codPesTextField.clear(); 
        }    
    }
    
    @FXML
    private void atualiza(ActionEvent event) {
        Pessoa l=tabela.getSelectionModel().getSelectedItem();
        tabela.getItems().remove(l);
        nomeTextField.setText(l.getNome());
        enderecoTextField.setText(l.getEndereco());
        idadeTextField.setText(""+l.getIdade()); //colocar "" para transformar tipo em string
        funcaoTextField.setText(l.getFuncao());
        codPesTextField.setText(""+l.getCodPes());
        if( l instanceof Aluno){
            Aluno a=(Aluno)l;
            alunoClicked();
            semestreTextField.setText(a.getSemestre());
            cursoTextField.setText(a.getCurso());
            
        }else if(l instanceof Professor){
            Professor p=(Professor)l;
            professorClicked();
            salarioTextField.setText(""+p.getSalario());
            disciplinaTextField.setText(p.getDisciplina());
        }else if (l instanceof FuncAdm){
            FuncAdm f=(FuncAdm)l;
            funcionarioClicked();
            salarioTextField.setText(""+f.getSalario());
            funcaoTextField.setText(f.getFuncao());
        }    
        l.update();
    }
    
    @FXML
    private void deleta(ActionEvent event) {
        Pessoa l=tabela.getSelectionModel().getSelectedItem();
        tabela.getItems().remove(l);
        l.delete();
    }

    @FXML
    private void deletaTudo(ActionEvent event) {
        tabela.getItems().clear();
        Pessoa.deleteall();
    }
 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tabela.getItems().clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        enderecoCol.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        idadeCol.setCellValueFactory(new PropertyValueFactory<>("idade"));
        funcaoCol.setCellValueFactory(new PropertyValueFactory<>("funcao"));  
        codPesCol.setCellValueFactory(new PropertyValueFactory<>("codPes"));  
            salarioTextField.setVisible(false);
            semestreTextField.setVisible(false);
            cursoTextField.setVisible(false);
            disciplinaTextField.setVisible(false); 
            salarioLabel.setVisible(false); 
            semestreLabel.setVisible(false); 
            cursoLabel.setVisible(false); 
            disciplinaLabel.setVisible(false);
            setorLabel.setVisible(false); 
            setorTextField.setVisible(false);
            funcaoLabel.setVisible(false); 
            funcaoTextField.setVisible(false);
            
            tabela.getItems().addAll(Professor.getAll());
            tabela.getItems().addAll(FuncAdm.getAll());
            tabela.getItems().addAll(Aluno.getAll());
        }          
    
    @FXML
    private void funcionarioClicked() {
            salarioTextField.setVisible(true);
            semestreTextField.setVisible(false);
            cursoTextField.setVisible(false);
            disciplinaTextField.setVisible(false);   
            salarioLabel.setVisible(true); 
            semestreLabel.setVisible(false); 
            cursoLabel.setVisible(false); 
            disciplinaLabel.setVisible(false); 
            setorLabel.setVisible(true); 
            setorTextField.setVisible(true);
            funcaoLabel.setVisible(true); 
            funcaoTextField.setVisible(true);
    }
    
    @FXML
    private void alunoClicked() {
            salarioTextField.setVisible(false);
            semestreTextField.setVisible(true);
            cursoTextField.setVisible(true);
            disciplinaTextField.setVisible(false); 
            salarioLabel.setVisible(false); 
            semestreLabel.setVisible(true); 
            cursoLabel.setVisible(true); 
            disciplinaLabel.setVisible(false);
            setorLabel.setVisible(false); 
            setorTextField.setVisible(false);
            funcaoLabel.setVisible(false); 
            funcaoTextField.setVisible(false);
    }

    @FXML
    private void professorClicked() {
            salarioTextField.setVisible(true);
            semestreTextField.setVisible(false);
            cursoTextField.setVisible(false);
            disciplinaTextField.setVisible(true); 
            salarioLabel.setVisible(true); 
            semestreLabel.setVisible(false); 
            cursoLabel.setVisible(false); 
            disciplinaLabel.setVisible(true); 
            setorLabel.setVisible(false); 
            setorTextField.setVisible(false);
            funcaoLabel.setVisible(false); 
            funcaoTextField.setVisible(false);
    }
       
    }